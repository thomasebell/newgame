varying vec3 normal;
varying vec3 vertexToLightVector;

void main() {
    const vec4 ambientColor = vec4(0.1, 0.0, 0.0, 1.0);
    const vec4 diffuseColor = vec4(1.0, 0.0, 0.0, 1.0);

    vec3 normalizedNormal = normalize(normal);
    vec3 normalizedVertexToLightVector = normalize(vertexToLightVector);

    float diffuseTerm = clamp(dot(normal, vertexToLightVector), 0.0, 1.0);

    gl_FragColor = ambientColor + diffuseColor * diffuseTerm;
}