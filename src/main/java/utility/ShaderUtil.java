/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;

/**
 *
 * @author thomas
 */
public class ShaderUtil {
    
    public static int loadShaders(String vertexShaderLocation, String fragmentShaderLocation) {
        int shaderProgram = glCreateProgram();
        int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        StringBuilder vertexShaderSource = new StringBuilder();
        StringBuilder fragmentShaderSource = new StringBuilder();
        BufferedReader reader = null;
        // load shader files
        try {
            reader = new BufferedReader(new FileReader(vertexShaderLocation));
            String line;
            while ((line = reader.readLine()) != null) {
                vertexShaderSource.append(line).append('\n');
            }
        } catch (IOException e) {
            Logger.getLogger(ShaderUtil.class.getName()).log(Level.SEVERE, e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Logger.getLogger(ShaderUtil.class.getName()).log(Level.SEVERE, e.getMessage()); 
                }
            }
        }
        
        try {
            reader = new BufferedReader(new FileReader(fragmentShaderLocation));
            String line;
            while ((line = reader.readLine()) != null) {
                fragmentShaderSource.append(line).append('\n');
            }
        } catch (IOException e) {
            Logger.getLogger(ShaderUtil.class.getName()).log(Level.SEVERE, e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Logger.getLogger(ShaderUtil.class.getName()).log(Level.SEVERE, e.getMessage()); 
                }
            }
        }

        glShaderSource(vertexShader, vertexShaderSource);
        glCompileShader(vertexShader);
        if (glGetShaderi(vertexShader, GL_COMPILE_STATUS) == GL_FALSE) {
            Logger.getLogger(ShaderUtil.class.getName()).log(Level.WARNING, "vertex shader compile failed");
        }
        glShaderSource(fragmentShader, fragmentShaderSource);
        glCompileShader(fragmentShader);
        if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) == GL_FALSE) {
            Logger.getLogger(ShaderUtil.class.getName()).log(Level.WARNING, "fragment shader compile failed");
        }
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        glLinkProgram(shaderProgram);
        
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        //glValidateProgram(shaderProgram);
        return shaderProgram;
    }
    
}
