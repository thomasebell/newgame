/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.nio.FloatBuffer;
import java.util.Locale;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

/**
 *
 * @author thomas
 */
public class TextUtil {
    
    private static int vertBuff;	
	
	public static void init(){
		FloatBuffer data = BufferUtils.createFloatBuffer(2);
		data.put(new float[]{0,0});
		data.flip();
		
		vertBuff = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vertBuff);
		glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public static void drawStringVbo(String s, int x, int y){
		int startX = x;
		
		glPushMatrix();
		glEnableClientState(GL_VERTEX_ARRAY);
		
		glBindBuffer(GL_ARRAY_BUFFER, vertBuff);
		glVertexPointer(2, GL_FLOAT, 0, 0L);
		
		for(char c : s.toLowerCase(Locale.ENGLISH).toCharArray()){
			if(c == 'a'){
				for(int i=0;i<8;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+8);
					point(x+i,y+4);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'b'){
				for(int i=0;i<8;i++){
					point(x+1,y+i);
				}
				for(int i=1;i<=6;i++){
					point(x+i,y);
					point(x+i,y+4);
					point(x+i,y+8);
				}
				for(int i = 1; i <=7; i++){
					point(x+7,y+i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'c'){
				for(int i=1;i<=7;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+0);
					point(x+i,y+8);
				}
				
				point(x+6,y+1);
				point(x+6,y+2);
				point(x+6,y+6);
				point(x+6,y+7);
				
				glTranslatef(8, 0, 0);
			}else if(c == 'd'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y);
					point(x+i,y+8);
				}
				for(int i = 1; i <= 7; i++){
					point(x+6,y+i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'e'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=1;i<=6;i++){
					point(x+i,y);
					point(x+i,y+8);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+4);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'f'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=1;i<=6;i++){
					point(x+i,y+8);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+4);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'g'){
				for(int i=1;i<=7;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+0);
					point(x+i,y+8);
				}
				for(int i = 1; i <= 7; i++){
					if(!(i==4)&&!(i==5)){
						point(x+6,y+i);
					}
				}
				point(x+5,y+3);
				point(x+7,y+3);
				
				glTranslatef(8, 0, 0);
			}else if(c == 'h'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+4);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'i'){
				for(int i=0;i<=8;i++){
					point(x+3,y+i);
				}
				for(int i=1;i<=5;i++){
					point(x+i,y);
					point(x+i,y+8);
				}
				glTranslatef(7, 0, 0);
			}else if(c == 'j'){
				for(int i=1;i<=8;i++){
					point(x+6,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+0);
				}
				for(int i = 1; i<=3; i++){
					point(x+1,y+i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'k'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i = 2; i <= 6; i++){
					point(x+i,y+i+2);
				}
				point(x+2,y+3);
				for(int i = 3; i <= 7; i++){
					point(x+i,y+7-i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'l'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=1;i<=6;i++){
					point(x+i,y+0);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'm'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i = 2; i <= 4; i++){
					point(x+i,y+9-i);
				}
				for(int i = 4; i <= 6; i++){
					point(x+i,y+i+1);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'n'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				point(x+2,y+7);
				for(int i = 2; i<=6; i++ ){
					point(x+i,y+8-i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'o' || c == '0'){
				for(int i=1;i<=7;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+8);
					point(x+i,y+0);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'p'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+8);
					point(x+i,y+4);
				}
				for(int i = 5; i <= 7; i++){
					point(x+6,y+i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'q'){
				for(int i=1;i<=7;i++){
					point(x+1,y+i);
					if(i != 1){
						point(x+7,y+i);
					}
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+8);
					if(i != 6) {
						point(x+i,y);
					}
				}
				for(int i = 4; i <= 7; i++){
					point(x+i,y+7-i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'r'){
				for(int i=0;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+8);
					point(x+i,y+4);
				}
				for(int i=5; i<= 7; i++){
					point(x+6,y+i);
				}
				for(int i = 4; i <= 7; i++){
					point(x+i,y+7-i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 's'){
				for(int i=2;i<=7;i++){
					point(x+i,y+8);
				}
				for(int i=1; i<=7; i++){
					if(!(i==3)&&!(i==4)){
						point(x+1,y+i);
					}
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+4);
					point(x+i,y);
				}
				for(int i = 1; i<=3; i++){
					point(x+7,y+i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 't'){
				for(int i=0;i<=8;i++){
					point(x+4,y+i);
				}
				for(int i=1;i<=7;i++){
					point(x+i,y+8);
				}
				glTranslatef(8,0,0);
			}else if(c == 'u'){
				for(int i=1;i<=8;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+0);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'v'){
				for(int i=2;i<=8;i++){
					point(x+1,y+i);
					point(x+6,y+i);
				}
				point(x+2,y+1);
				point(x+5,y+1);
				point(x+3,y);
				point(x+4,y);
				
				glTranslatef(8, 0, 0);
			}else if(c == 'w'){
				for(int i=1;i<=8;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i=1;i<=6;i++){
					point(x+4,y+i);
					if(!(i==1)&&!(i==4)){
						point(x+i,y);
					}
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'x'){
				for(int i=1;i<=7;i++){
					point(x+i,y+i);
				}
					
				for(int i=7;i>=1;i--){
					point(x+i,y+8-i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == 'y'){
				for(int i = 0; i<=4; i++){
					point(x+4,y+i);
				}
				point(x+3,y+5);
				point(x+2,y+6);
				point(x+1,y+7);
				point(x+1,y+8);
				
				point(x+5,y+5);
				point(x+6,y+6);
				point(x+7,y+7);
				point(x+7,y+8);
				
				glTranslatef(8, 0, 0);
			}else if(c == 'z'){
				for(int i=1;i<=6;i++){
					point(x+i,y);
					point(x+i,y+8);
					point(x+i,y+i);
				}
				point(x+6,y+7);
				
				glTranslatef(8, 0, 0);
			}else if(c == '1'){
				for(int i=2;i<=6;i++){
					point(x+i,y);
				}
				for(int i=1;i<=8;i++){
					point(x+4,y+i);
				}
				point(x+3,y+7);
				glTranslatef(8, 0, 0);
			}else if(c == '2'){
				for(int i=1;i<=6;i++){
					point(x+i,y);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+8);
				}
				point(x+1,y+7);
				point(x+1,y+6);
				for(int i = 1; i<=7; i++){
					if(i<=4){
						point(x+i+1,y+i);
					}else{
						point(x+6,y+i);
					}
				}
				glTranslatef(8, 0, 0);
			}else if(c == '3'){
				for(int i=1;i<=5;i++){
					point(x+i,y+8);
					point(x+i,y);
				}
				for(int i=1;i<=7;i++){
					point(x+6,y+i);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+4);
				}
				glTranslatef(8, 0, 0);
			}else if(c == '4'){
				for(int i=2;i<=8;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=7;i++){
					point(x+i,y+1);
				}
				for(int i=0;i<=4;i++){
					point(x+4,y+i);
				}
				glTranslatef(8, 0, 0);
			}else if(c == '5'){
				for(int i=1;i<=7;i++){
					point(x+i,y+8);
				}
				for(int i=4;i<=7;i++){
					point(x+1,y+i);
				}
				point(x+1,y+1);
				point(x+2,y);
				point(x+3,y);
				point(x+4,y);
				point(x+5,y);
				point(x+6,y);
				
				point(x+7,y+1);
				point(x+7,y+2);
				point(x+7,y+3);
				
				point(x+6,y+4);
				point(x+5,y+4);
				point(x+4,y+4);
				point(x+3,y+4);
				point(x+2,y+4);
				
				glTranslatef(8, 0, 0);
			}else if(c == '6'){
				for(int i=1;i<=7;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y);
				}
				for(int i=2;i<=5;i++){
					point(x+i,y+4);
					point(x+i,y+8);
				}
				point(x+7,y+1);
				point(x+7,y+2);
				point(x+7,y+3);
				point(x+6,y+4);
				glTranslatef(8, 0, 0);
			}else if(c == '7'){
				for(int i=0;i<=7;i++)
					point(x+i, y+8);
				
				point(x+7, y+7);
				point(x+7, y+6);
				
				point(x+6, y+5);
				point(x+5, y+4);
				point(x+4, y+3);
				point(x+3, y+2);
				point(x+2, y+1);
				point(x+1, y);
				
				glTranslatef(8, 0, 0);
			}else if(c == '8'){
				for(int i=1;i<=7;i++){
					point(x+1,y+i);
					point(x+7,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+8);
					point(x+i,y+0);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+4);
				}
				glTranslatef(8, 0, 0);
			}else if(c == '9'){
				for(int i=1;i<=7;i++){
					point(x+7,y+i);
				}
				for(int i=5;i<=7;i++){
					point(x+1,y+i);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+8);
					point(x+i,y+0);
				}
				for(int i=2;i<=6;i++){
					point(x+i,y+4);
				}
				point(x+1,y);
				glTranslatef(8, 0, 0);
			}else if(c == '.'){
				point(x+1,y);
				glTranslatef(2, 0, 0);
			}else if(c == ','){
				point(x+1,y);
				point(x+1,y+1);
				glTranslatef(2, 0, 0);
			}else if(c == '\n'){
				y-=10;
				x = startX;
			}else if(c == ' '){
				glTranslatef(8, 0, 0);
			}else if(c == '>'){
				point(x+1,y+1);
				point(x+2,y+2);
				point(x+3,y+3);
				point(x+4,y+4);
				point(x+3,y+5);
				point(x+2,y+6);
				point(x+1,y+7);
				glTranslatef(8, 0, 0);
			}else if(c == '<'){
				point(x+4,y+1);
				point(x+3,y+2);
				point(x+2,y+3);
				point(x+1,y+4);
				point(x+2,y+5);
				point(x+3,y+6);
				point(x+4,y+7);
				glTranslatef(8, 0, 0);
			}else if(c == ':'){
				point(x+3,y+1);
				point(x+3,y+5);
				glTranslatef(5, 0, 0);
			}
		}
		glDisableClientState(GL_VERTEX_ARRAY);
		glPopMatrix();
	}
	
	private static void point(int a, int b){
		glPushMatrix();
		glTranslatef(a, b, 0);
		glDrawArrays(GL_POINTS, 0, 1);
		glPopMatrix();
	}
	
	public static void destroy(){
		glDeleteBuffers(vertBuff);
	}
        
        public static void drawString(String s, int x, int y){
		int startX = x;
		GL11.glBegin(GL11.GL_LINES);
		for(char c : s.toLowerCase().toCharArray()){
			if(c == 'a'){
				for(int i=0;i<8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+4);
				}
				x+=8;
			}else if(c == 'b'){
				for(int i=0;i<8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+i, y);
					GL11.glVertex2f(x+i, y+4);
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+7, y+5);
				GL11.glVertex2f(x+7, y+7);
				GL11.glVertex2f(x+7, y+6);
				
				GL11.glVertex2f(x+7, y+1);
				GL11.glVertex2f(x+7, y+2);
				GL11.glVertex2f(x+7, y+3);
				x+=8;
			}else if(c == 'c'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y);
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+6, y+1);
				GL11.glVertex2f(x+6, y+2);
				
				GL11.glVertex2f(x+6, y+6);
				GL11.glVertex2f(x+6, y+7);
				
				x+=8;
			}else if(c == 'd'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y);
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+6, y+1);
				GL11.glVertex2f(x+6, y+2);
				GL11.glVertex2f(x+6, y+3);
				GL11.glVertex2f(x+6, y+4);
				GL11.glVertex2f(x+6, y+5);
				GL11.glVertex2f(x+6, y+6);
				GL11.glVertex2f(x+6, y+7);
				
				x+=8;
			}else if(c == 'e'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+i, y+0);
					GL11.glVertex2f(x+i, y+8);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+4);
				}
				x+=8;
			}else if(c == 'f'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+i, y+8);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+4);
				}
				x+=8;
			}else if(c == 'g'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y);
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+6, y+1);
				GL11.glVertex2f(x+6, y+2);
				GL11.glVertex2f(x+6, y+3);
				GL11.glVertex2f(x+5, y+3);
				GL11.glVertex2f(x+7, y+3);
				
				GL11.glVertex2f(x+6, y+6);
				GL11.glVertex2f(x+6, y+7);
				
				x+=8;
			}else if(c == 'h'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+4);
				}
				x+=8;
			}else if(c == 'i'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+3, y+i);
				}
				for(int i=1;i<=5;i++){
					GL11.glVertex2f(x+i, y+0);
					GL11.glVertex2f(x+i, y+8);
				}
				x+=7;
			}else if(c == 'j'){
				for(int i=1;i<=8;i++){
					GL11.glVertex2f(x+6, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+0);
				}
				GL11.glVertex2f(x+1, y+3);
				GL11.glVertex2f(x+1, y+2);
				GL11.glVertex2f(x+1, y+1);
				x+=8;
			}else if(c == 'k'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				GL11.glVertex2f(x+6, y+8);
				GL11.glVertex2f(x+5, y+7);
				GL11.glVertex2f(x+4, y+6);
				GL11.glVertex2f(x+3, y+5);
				GL11.glVertex2f(x+2, y+4);
				GL11.glVertex2f(x+2, y+3);
				GL11.glVertex2f(x+3, y+4);
				GL11.glVertex2f(x+4, y+3);
				GL11.glVertex2f(x+5, y+2);
				GL11.glVertex2f(x+6, y+1);
				GL11.glVertex2f(x+7, y);
				x+=8;
			}else if(c == 'l'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+i, y);
				}
				x+=7;
			}else if(c == 'm'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				GL11.glVertex2f(x+3, y+6);
				GL11.glVertex2f(x+2, y+7);
				GL11.glVertex2f(x+4, y+5);
				
				GL11.glVertex2f(x+5, y+6);
				GL11.glVertex2f(x+6, y+7);
				GL11.glVertex2f(x+4, y+5);
				x+=8;
			}else if(c == 'n'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				GL11.glVertex2f(x+2, y+7);
				GL11.glVertex2f(x+2, y+6);
				GL11.glVertex2f(x+3, y+5);
				GL11.glVertex2f(x+4, y+4);
				GL11.glVertex2f(x+5, y+3);
				GL11.glVertex2f(x+6, y+2);
				GL11.glVertex2f(x+6, y+1);
				x+=8;
			}else if(c == 'o' || c == '0'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+0);
				}
				x+=8;
			}else if(c == 'p'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+4);
				}
				GL11.glVertex2f(x+6, y+7);
				GL11.glVertex2f(x+6, y+5);
				GL11.glVertex2f(x+6, y+6);
				x+=8;
			}else if(c == 'q'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
					if(i != 1) GL11.glVertex2f(x+7, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+8);
					if(i != 6) GL11.glVertex2f(x+i, y+0);
				}
				GL11.glVertex2f(x+4, y+3);
				GL11.glVertex2f(x+5, y+2);
				GL11.glVertex2f(x+6, y+1);
				GL11.glVertex2f(x+7, y);
				x+=8;
			}else if(c == 'r'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+4);
				}
				GL11.glVertex2f(x+6, y+7);
				GL11.glVertex2f(x+6, y+5);
				GL11.glVertex2f(x+6, y+6);
				
				GL11.glVertex2f(x+4, y+3);
				GL11.glVertex2f(x+5, y+2);
				GL11.glVertex2f(x+6, y+1);
				GL11.glVertex2f(x+7, y);
				x+=8;
			}else if(c == 's'){
				for(int i=2;i<=7;i++){
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+1, y+7);
				GL11.glVertex2f(x+1, y+6);
				GL11.glVertex2f(x+1, y+5);
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+4);
					GL11.glVertex2f(x+i, y);
				}
				GL11.glVertex2f(x+7, y+3);
				GL11.glVertex2f(x+7, y+2);
				GL11.glVertex2f(x+7, y+1);
				GL11.glVertex2f(x+1, y+1);
				GL11.glVertex2f(x+1, y+2);
				x+=8;
			}else if(c == 't'){
				for(int i=0;i<=8;i++){
					GL11.glVertex2f(x+4, y+i);
				}
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+i, y+8);
				}
				x+=7;
			}else if(c == 'u'){
				for(int i=1;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+0);
				}
				x+=8;
			}else if(c == 'v'){
				for(int i=2;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+6, y+i);
				}
				GL11.glVertex2f(x+2, y+1);
				GL11.glVertex2f(x+5, y+1);
				GL11.glVertex2f(x+3, y);
				GL11.glVertex2f(x+4, y);
				x+=7;
			}else if(c == 'w'){
				for(int i=1;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				GL11.glVertex2f(x+2, y);
				GL11.glVertex2f(x+3, y);
				GL11.glVertex2f(x+5, y);
				GL11.glVertex2f(x+6, y);
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+4, y+i);
				}
				x+=8;
			}else if(c == 'x'){
				for(int i=1;i<=7;i++)
					GL11.glVertex2f(x+i, y+i);
				for(int i=7;i>=1;i--)
					GL11.glVertex2f(x+i, y+8-i);
				x+=8;
			}else if(c == 'y'){
				GL11.glVertex2f(x+4, y);
				GL11.glVertex2f(x+4, y+1);
				GL11.glVertex2f(x+4, y+2);
				GL11.glVertex2f(x+4, y+3);
				GL11.glVertex2f(x+4, y+4);
				
				GL11.glVertex2f(x+3, y+5);
				GL11.glVertex2f(x+2, y+6);
				GL11.glVertex2f(x+1, y+7);
				GL11.glVertex2f(x+1, y+8);
				
				GL11.glVertex2f(x+5, y+5);
				GL11.glVertex2f(x+6, y+6);
				GL11.glVertex2f(x+7, y+7);
				GL11.glVertex2f(x+7, y+8);
				x+=8;
			}else if(c == 'z'){
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+i, y);
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+i);
				}
				GL11.glVertex2f(x+6, y+7);
				x += 8;
			}else if(c == '1'){
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y);
				}
				for(int i=1;i<=8;i++){
					GL11.glVertex2f(x+4, y+i);
				}
				GL11.glVertex2f(x+3, y+7);
				x += 8;
			}else if(c == '2'){
				for(int i=1;i<=6;i++){
					GL11.glVertex2f(x+i, y);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+1, y+7);
				GL11.glVertex2f(x+1, y+6);
				
				GL11.glVertex2f(x+6, y+7);
				GL11.glVertex2f(x+6, y+6);
				GL11.glVertex2f(x+6, y+5);
				GL11.glVertex2f(x+5, y+4);
				GL11.glVertex2f(x+4, y+3);
				GL11.glVertex2f(x+3, y+2);
				GL11.glVertex2f(x+2, y+1);
				x += 8;
			}else if(c == '3'){
				for(int i=1;i<=5;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y);
				}
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+6, y+i);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+4);
				}
				x += 8;
			}else if(c == '4'){
				for(int i=2;i<=8;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=7;i++){
					GL11.glVertex2f(x+i, y+1);
				}
				for(int i=0;i<=4;i++){
					GL11.glVertex2f(x+4, y+i);
				}
				x+=8;
			}else if(c == '5'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+i, y+8);
				}
				for(int i=4;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				GL11.glVertex2f(x+1, y+1);
				GL11.glVertex2f(x+2, y);
				GL11.glVertex2f(x+3, y);
				GL11.glVertex2f(x+4, y);
				GL11.glVertex2f(x+5, y);
				GL11.glVertex2f(x+6, y);
				
				GL11.glVertex2f(x+7, y+1);
				GL11.glVertex2f(x+7, y+2);
				GL11.glVertex2f(x+7, y+3);
				
				GL11.glVertex2f(x+6, y+4);
				GL11.glVertex2f(x+5, y+4);
				GL11.glVertex2f(x+4, y+4);
				GL11.glVertex2f(x+3, y+4);
				GL11.glVertex2f(x+2, y+4);
				x += 8;
			}else if(c == '6'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y);
				}
				for(int i=2;i<=5;i++){
					GL11.glVertex2f(x+i, y+4);
					GL11.glVertex2f(x+i, y+8);
				}
				GL11.glVertex2f(x+7, y+1);
				GL11.glVertex2f(x+7, y+2);
				GL11.glVertex2f(x+7, y+3);
				GL11.glVertex2f(x+6, y+4);
				x+=8;
			}else if(c == '7'){
				for(int i=0;i<=7;i++)
					GL11.glVertex2f(x+i, y+8);
				GL11.glVertex2f(x+7, y+7);
				GL11.glVertex2f(x+7, y+6);
				
				GL11.glVertex2f(x+6, y+5);
				GL11.glVertex2f(x+5, y+4);
				GL11.glVertex2f(x+4, y+3);
				GL11.glVertex2f(x+3, y+2);
				GL11.glVertex2f(x+2, y+1);
				GL11.glVertex2f(x+1, y);
				x+=8;
			}else if(c == '8'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
					GL11.glVertex2f(x+7, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+0);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+4);
				}
				x += 8;
			}else if(c == '9'){
				for(int i=1;i<=7;i++){
					GL11.glVertex2f(x+7, y+i);
				}
				for(int i=5;i<=7;i++){
					GL11.glVertex2f(x+1, y+i);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+8);
					GL11.glVertex2f(x+i, y+0);
				}
				for(int i=2;i<=6;i++){
					GL11.glVertex2f(x+i, y+4);
				}
				GL11.glVertex2f(x+1, y+0);
				x += 8;
			}else if(c == '.'){
				GL11.glVertex2f(x+1, y);
				x+=2;
			}else if(c == ','){
				GL11.glVertex2f(x+1, y);
				GL11.glVertex2f(x+1, y+1);
				x+=2;
			}else if(c == '\n'){
				y-=10;
				x = startX;
			}else if(c == ' '){
				x += 8;
			}
		}
		GL11.glEnd();
        }
    
}
