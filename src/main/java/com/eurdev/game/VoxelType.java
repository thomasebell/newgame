/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eurdev.game;

/**
 *
 * @author thomas
 */
public enum VoxelType {
    
    DEFAULT(0),
    GROUND(1);
    
    private int id;
    
    VoxelType(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
}
