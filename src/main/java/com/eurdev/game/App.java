package com.eurdev.game;

import de.matthiasmann.twl.utils.PNGDecoder;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import org.lwjgl.util.vector.Vector3f;
import utility.ShaderUtil;
import utility.TextUtil;

/**
 * Hello world!
 *
 */
public class App {
    
    final static int xResolution = 800;
    final static int yResolution = 600;
    final static int xFullscreenResolution = 1600;
    final static int yFullscreenResolution = 900;
    private static boolean finished;
    private static Logger LOGGER = Logger.getLogger(App.class.getName());
    
    
    // time at last frame
    long lastFrame;
    int framesPerSecond;
    long lastFramesPerSecond;
    
    boolean vsync;
    
    float mouseSensitivity = 0.08f;
    boolean mouseGrab;
    
    // shader program
    int shaderProgram;
        
    Camera camera;
    Cube player;
    
    Cube[][][] cubeArray;
    Plane ground;
    
    float[][] data;
    static int lookupTexture;
    static int heightMapDisplayList;
    
    private VoxelGroup[] voxelGroups;
    
    private boolean lightingEnabled;
    
    
    
    
    public void start() {
        
        initDisplay();
        initGL();
        
        // main game loop
        gameLoop(); 
        
        cleanUp();
    }
    
    private void initDisplay() {
        try {        
            Display.setDisplayMode(new DisplayMode(xResolution, yResolution));
            Display.setResizable(true);
            Display.create();
        } catch (LWJGLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } 
    }
    
    private void initGL() {
        
        
        initStates();
        initObjects();
        initShaders();
        initCamera();
        
        LOGGER.log(Level.INFO, "aspect ratio: " + camera.getAspectRatio());
        
        
    }
    
    private void initStates() {
        glPointSize(1);
        glEnable(GL_DEPTH_TEST);
        
        glEnable(GL_LINE_SMOOTH);
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glLineWidth(0.5f);
        
        glShadeModel(GL_SMOOTH);

        //glClearColor(0, 0.75f, 1, 1);
        //glEnable(GL_CULL_FACE);
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
    }
    
    private void initObjects() {
        voxelGroups = new VoxelGroup[3];
        int count = 0;
        for (int i = 0; i < voxelGroups.length; i++) {
            voxelGroups[i] = new VoxelGroup(new Vector3f(count, 0, 0), VoxelType.DEFAULT);
            count += 75;
        }

        //buildHeightMap();
        //cube = new Cube(new Vector3f(0.0f, 0.0f, -10f));
//        List<Float> groundColor = new ArrayList<Float>(Arrays.asList(255.0f, 0.0f, 0.0f));
//        List<Float> playerColor = new ArrayList<Float>(Arrays.asList(0.0f, 0.0f, 255.0f));
//        cubeArray = new Cube[100][1][100];
//        for (int x = 0; x < 100; x++) {
//            for (int y = 0; y < 1; y++) {
//                for (int z = 0; z < 100; z++) {
//                    cubeArray[x][y][z] = new Cube(new Vector3f(x, y, z), groundColor);
//                }
//            }
//        }
//        
//        player = new Cube(new Vector3f(50.0f, 1.0f, 1.0f), playerColor);
        

    }
    
    private void initShaders() {
        shaderProgram = ShaderUtil.loadShaders("src/main/resources/shaders/vertex.glsl", "src/main/resources/shaders/fragment.glsl");
        glUseProgram(shaderProgram);
        //glUniform1i(glGetUniformLocation(shaderProgram, "lookup"), 0);
    }
    
    private void initCamera() {
        float width = Display.getWidth();
        float height = Display.getHeight();
        
        if (camera != null) {
            camera = new Camera(camera.getPosition(), camera.getYaw(), camera.getFieldOfView(), 0.3f, 1000, width, height); 
            LOGGER.log(Level.INFO, "camera position : " + ", yaw: " + camera.getYaw() + 
                    camera.getPosition().x + ", " + camera.getPosition().y + ", " + camera.getPosition().z);
            camera.setPitch(camera.getPitch());
            
        } else {
            //camera = new Camera(new Vector3f(-100.0f, -40.0f, 10.0f), 180, 70, 0.3f, 1000, width, height);
            camera = new Camera(new Vector3f(0.0f, 0.0f, 0.0f), 180, 45, 0.3f, 1000, width, height);
            camera.setPitch(45.0f);
        }
        
    }
    
    private void gameLoop() {
        getDelta();
        lastFramesPerSecond = getTime();
        
        while (!finished) {
            
            if (Display.isCloseRequested()) {
                finished = true;
            }
            
            int delta = getDelta();
            
            update(delta);
            // render OpenGL
            renderGL();

            Display.update();
            Display.sync(60); // 60 fps cap
        }
        
    }
    
    private void cleanUp() {
        glUseProgram(0);
        glDeleteProgram(shaderProgram);
        
        //glDeleteLists(heightMapDisplayList, 1);
        //glBindTexture(GL_TEXTURE_2D, 0);
        //glDeleteTextures(lookupTexture);
        
        for (VoxelGroup group : voxelGroups) {
            group.cleanUp();
        }
        Display.destroy();
    }
    
    private void update(int delta) {
        // rotate cube
//        for (int x = 0; x < 100; x++) {
//            for (int y = 0; y < 1; y++) {
//                for (int z = 0; z < 100; z++) {
//                    cubeArray[x][y][z].rotate(delta);
//                }
//            }
//        }
        
        if (Mouse.isButtonDown(1)) {
            camera.movePitch(-Mouse.getDY() * mouseSensitivity);
            camera.moveYaw(Mouse.getDX() * mouseSensitivity);   
        }
        
        
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            camera.moveLeft(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            camera.moveRight(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
            camera.turnLeft(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_E)) {
            camera.turnRight(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            //quad1.moveNorth(delta);
            camera.moveFoward(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            //quad1.moveSouth(delta);
            camera.moveBackward(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_EQUALS)) {
            //quad1.moveSouth(delta);
            camera.moveUp(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_MINUS)) {
            //quad1.moveSouth(delta);
            camera.moveDown(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
            player.moveForward(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
            player.moveBackward(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
            player.moveLeft(delta);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
            player.moveRight(delta);
        }
        
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                
                // toggle fullscreen/vsync
                if (Keyboard.getEventKey() == Keyboard.KEY_F) {
                    if (!Display.isFullscreen()) {
                        setDisplayMode(xFullscreenResolution, yFullscreenResolution, true);  
                    } else {
                        setDisplayMode(xResolution, yResolution, false);
                    }
                } else if (Keyboard.getEventKey() == Keyboard.KEY_V) {
                    vsync = !vsync;
                    Display.setVSyncEnabled(vsync);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_M) {
                    mouseGrab = !mouseGrab;
                    Mouse.setGrabbed(mouseGrab);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                    finished = true;
                } else if (Keyboard.getEventKey() == Keyboard.KEY_1) {
                    glPolygonMode(GL_FRONT, GL_FILL);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_2) {
                    glPolygonMode(GL_FRONT, GL_LINE);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_3) {
                    glPolygonMode(GL_FRONT, GL_POINT);
                } else if (Keyboard.getEventKey() == Keyboard.KEY_L) {
                    lightingEnabled = !lightingEnabled;

                    if (lightingEnabled) {
                        glDisable(GL_LIGHTING);
                    } else {
                        glEnable(GL_LIGHTING);
                    }
                } else if (Keyboard.getEventKey() == Keyboard.KEY_R) {
                    for (VoxelGroup group : voxelGroups) {
                        group.buildMesh(group.getStartPosition());
                    }
                } else if (Keyboard.getEventKey() == Keyboard.KEY_C) {
                    for (VoxelGroup group : voxelGroups) {
                        group.setupSphere();
                    }
                } else if (Keyboard.getEventKey() == Keyboard.KEY_X) {
                    for (VoxelGroup group : voxelGroups) {
                        group.setUpLandscape();
                    }
                }
            }
        }
        
        //detectCollisions();
        
        updateFramesPerSecond();
        
        
    }
    
    private void detectCollisions() {
        // keep quad on the screen
//        if (quad1.getX() < 0) {
//            quad1.setX(0);
//        }
//        if (quad1.getX() > xResolution) {
//            quad1.setX(xResolution);
//        }
//        if (quad1.getY() < 0) {
//            quad1.setY(0);
//        }
//        if (quad1.getY() > yResolution) {
//            quad1.setY(yResolution);
//        }
        
    }

    private int getDelta() {
        long time = getTime();
        int delta = (int) (time - lastFrame);
        lastFrame = time;
        
        return delta;
    }
    
//    public long getTime() {
//        return System.nanoTime() / 1000000;
//    }
    
    private long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }
    
    private void updateFramesPerSecond() {
        if (getTime() - lastFramesPerSecond > 1000) {
            //Display.setTitle("FPS: " + framesPerSecond);
            //TextUtil.drawString("Hello", 25, 25);
            framesPerSecond = 0;
            lastFramesPerSecond += 1000;
        }
        framesPerSecond++;
    }
    
    private void renderGL() {
        
        // clear screen and depth buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //glClearColor(0, 0.75f, 1, 1);
        glLoadIdentity();
        
        //glEnable(GL_CULL_FACE);

        // update camera view
        camera.useView();
        
//        cube.draw();
        glUseProgram(shaderProgram);
        for (VoxelGroup group : voxelGroups) {
            group.render();
        }
        glUseProgram(0);
            
        
//        for (int x = 0; x < 100; x++) {
//            for (int y = 0; y < 1; y++) {
//                for (int z = 0; z < 100; z++) {
//                    cubeArray[x][y][z].draw();
//                }
//            }
//        }
//        player.draw();
        
        //glCallList(heightMapDisplayList);
    }
    
    private void drawQuad(float xPosition, float yPosition) {
        

    }
    
    private void setDisplayMode(int width, int height, boolean fullscreen) {
        
        // check if requested DisplayMode is alread set
        DisplayMode displayMode = Display.getDisplayMode();
        DisplayMode desktopDisplayMode = Display.getDesktopDisplayMode();
        if ((displayMode.getWidth() == width) &&
                (displayMode.getHeight() == height) &&
                (Display.isFullscreen() == fullscreen)) {
            return;
        }
        
        try {
            DisplayMode targetDisplayMode = null;
            
            if (fullscreen) {
                DisplayMode[] displayModes = Display.getAvailableDisplayModes();
                int frequency = 0;
                
                for (int i = 0; i < displayModes.length; i++) {
                    DisplayMode current = displayModes[i];
                    
                    if ((current.getWidth() == width) && (current.getHeight() == height)) {
                        if ((targetDisplayMode == null) || (current.getFrequency() >= frequency)) {
                            if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
                                targetDisplayMode = current;
                                frequency = targetDisplayMode.getFrequency();
                            }
                        }
                        
                        if ((current.getBitsPerPixel() == desktopDisplayMode.getBitsPerPixel()) &&
                                (current.getFrequency() == desktopDisplayMode.getFrequency())) {
                            targetDisplayMode = current;
                            break;
                        }
                    }
                }
            } else {
                targetDisplayMode = new DisplayMode(width, height);
            }
            
            if (targetDisplayMode == null) {
                LOGGER.log(Level.WARNING, "Failed to find mode: " + width + "x" + height + 
                        " fullscreen=" + fullscreen);
                return;
            }
            LOGGER.log(Level.INFO, "Setting display mode: " + targetDisplayMode.getWidth() + "x" +
                    targetDisplayMode.getHeight());
            Display.setDisplayMode(targetDisplayMode);
            initCamera();
            Display.setFullscreen(fullscreen);
        } catch (LWJGLException e) {
            LOGGER.log(Level.SEVERE, "Exception setting mode: " + width + "x" + height + 
                    " fullscreen=" + fullscreen);
        }
    }
    
    private void buildHeightMap() {
        try {
            BufferedImage heightMapImage = ImageIO.read(new File("src/main/resources/images/latin_heightmap.bmp"));
            
            data = new float[heightMapImage.getWidth()][heightMapImage.getHeight()];
            
            Color color;
            
            for (int z = 0; z < data.length; z++) {
                for (int x = 0; x < data[z].length; x++) {
                    color = new Color(heightMapImage.getRGB(z, x));
                    
                    data[z][x] = color.getRed();
                }
            }
            
            FileInputStream heightMapLookupInputStream = new FileInputStream("src/main/resources/images/lookup.png");
            PNGDecoder pngDecoder = new PNGDecoder(heightMapLookupInputStream);
            
            ByteBuffer byteBuffer = BufferUtils.createByteBuffer(4 * pngDecoder.getWidth() * pngDecoder.getHeight());
            
            pngDecoder.decode(byteBuffer, pngDecoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            
            byteBuffer.flip();
            heightMapLookupInputStream.close();
            
            lookupTexture = glGenTextures();
            glBindTexture(GL_TEXTURE_2D, lookupTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pngDecoder.getWidth(), pngDecoder.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, byteBuffer);
            
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Exception during buildHeightMap: " + e.getMessage());  
        }
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        
        heightMapDisplayList = glGenLists(1);
        
        glNewList(heightMapDisplayList, GL_COMPILE);
        glScalef(0.2f, 0.06f, 0.2f);
        
        // iterate over the strips of heightmap data
        for (int z = 0; z < data.length - 1; z++) {
            
            glBegin(GL_TRIANGLE_STRIP);
            for (int x = 0; x < data[z].length; x++) {
                glVertex3f(x, data[z][x], z);
                glVertex3f(x, data[z+1][x], z + 1);
            }
            glEnd();
        }
        glEndList();
        
        
    }

    public static void main(String[] args) {
        new App().start();
    }
}
