/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eurdev.game;
import java.nio.FloatBuffer;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Vector3f;
import utility.SimplexNoise;

/**
 *
 * @author thomas
 */
public class VoxelGroup extends RenderedObject {
    
    static final int VOXEL_GROUP_SIZE = 32;
    static final int VOXEL_SIZE = 2;
    private static Logger LOGGER = Logger.getLogger(App.class.getName());
    private Voxel[][][] voxels;
    private VoxelType type;
    
    private int vboVertextHandle;
    private int vboColorHandle;
    private Vector3f startPosition; 
    
    private Random random;

    public VoxelGroup(Vector3f startPosition, VoxelType type) {
        this.type = type;
        random = new Random();
        
        voxels = new Voxel[VOXEL_GROUP_SIZE][VOXEL_GROUP_SIZE][VOXEL_GROUP_SIZE];
        for (int x = 0; x < VOXEL_GROUP_SIZE; x++) {
            for (int y = 0; y < VOXEL_GROUP_SIZE; y++) {
                for (int z = 0; z < VOXEL_GROUP_SIZE; z++) {
                    if (random.nextFloat() > 0.7f) {
                        this.type = VoxelType.DEFAULT; 
                    } else {
                        this.type = VoxelType.GROUND;
                    }
                    voxels[x][y][z] = new Voxel(this.type);
                    voxels[x][y][z].setActive(true);
                }
            }
        }
        
        vboColorHandle = glGenBuffers();
        vboVertextHandle = glGenBuffers();
        this.startPosition = startPosition;
        update();
    }
    
    public void buildMesh(Vector3f position) {
        this.startPosition = position;
        vboColorHandle = glGenBuffers();
        vboVertextHandle = glGenBuffers();
        
        long time = System.nanoTime();
        
        FloatBuffer vertexPositionData = BufferUtils.createFloatBuffer((VOXEL_GROUP_SIZE * VOXEL_GROUP_SIZE * VOXEL_GROUP_SIZE) * 6 * 12);
        FloatBuffer vertexColorData = BufferUtils.createFloatBuffer((VOXEL_GROUP_SIZE * VOXEL_GROUP_SIZE * VOXEL_GROUP_SIZE) * 6 * 12);
        
        for (float x = 0; x < VOXEL_GROUP_SIZE; x += 1) {
            for (float y = 0; y < VOXEL_GROUP_SIZE; y += 1) {
                for (float z = 0; z < VOXEL_GROUP_SIZE; z += 1) {
                    
                        
                    int xVoxelIndex = (int) (x);
                    int yVoxelIndex = (int) (y);
                    int zVoxelIndex = (int) (z);  
                    final Voxel currentVoxel = voxels[xVoxelIndex][yVoxelIndex][zVoxelIndex];
                    
                    boolean voxelX1Active = false;
                    boolean voxelX2Active = false;
                    boolean voxelY1Active = false;
                    boolean voxelY2Active = false;
                    boolean voxelZ1Active = false;
                    boolean voxelZ2Active = false;
                    
                    if (x > 0)
                        voxelX1Active = voxels[xVoxelIndex - 1][yVoxelIndex][zVoxelIndex].isActive();
                    
                    
                    if (x < VOXEL_GROUP_SIZE - 1)
                        voxelX2Active = voxels[xVoxelIndex + 1][yVoxelIndex][zVoxelIndex].isActive();
                    
                    if (y > 0)
                        voxelY1Active = voxels[xVoxelIndex][yVoxelIndex - 1][zVoxelIndex].isActive();
                    
                    if (y < VOXEL_GROUP_SIZE - 1)
                        voxelY2Active = voxels[xVoxelIndex][yVoxelIndex + 1][zVoxelIndex].isActive();
                    
                    if (z > 0)
                        voxelZ1Active = voxels[xVoxelIndex][yVoxelIndex][zVoxelIndex - 1].isActive();
                    
                    if (z < VOXEL_GROUP_SIZE - 1)
                        voxelZ2Active = voxels[xVoxelIndex][yVoxelIndex][zVoxelIndex + 1].isActive();
                    
                    boolean voxelIsVisible = !(voxelX1Active && voxelX2Active && voxelY1Active && voxelY2Active 
                            && voxelZ1Active && voxelZ2Active);
                    
                    //LOGGER.log(Level.INFO, "voxel visible: " + voxelX1Active + " " + voxelX2Active + " " + voxelY1Active + " " + voxelY2Active + " " + voxelZ1Active + " " + voxelZ2Active );
                    
                    
                    if (currentVoxel.isActive() && voxelIsVisible) {
                        final Vector3f currentPosition = new Vector3f(startPosition.x + x * VOXEL_SIZE,
                                startPosition.y + y * VOXEL_SIZE,
                                startPosition.z + z * VOXEL_SIZE);
                        vertexPositionData.put(createVoxelData(currentPosition));

                        final float[] voxelVertexColor = createVoxelVertexColor(getVoxelColor(currentVoxel));
                        vertexColorData.put(voxelVertexColor);
                    }
                    
                }
            }
        }
        
        vertexColorData.flip();
        vertexPositionData.flip();
        
        glBindBuffer(GL_ARRAY_BUFFER, vboVertextHandle);
        glBufferData(GL_ARRAY_BUFFER, vertexPositionData, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindBuffer(GL_ARRAY_BUFFER, vboColorHandle);
        glBufferData(GL_ARRAY_BUFFER, vertexColorData, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);  
        
    }
    
    // creates individual voxel based on position (center of voxel)
    public static float[] createVoxelData(Vector3f position) {
        float[] voxel = new float[]{
            // front face
            position.x - 1, position.y - 1, position.z - 1,
            position.x - 1, position.y + 1, position.z - 1,
            position.x + 1, position.y + 1, position.z - 1,
            position.x + 1, position.y - 1, position.z - 1,
            // left face
            position.x - 1, position.y - 1, position.z + 1,
            position.x - 1, position.y + 1, position.z + 1,
            position.x - 1, position.y + 1, position.z - 1,
            position.x - 1, position.y - 1, position.z - 1,
            // back face
            position.x + 1, position.y - 1, position.z + 1,
            position.x + 1, position.y + 1, position.z + 1,
            position.x - 1, position.y + 1, position.z + 1,
            position.x - 1, position.y - 1, position.z + 1,
            // right face
            position.x + 1, position.y - 1, position.z - 1,
            position.x + 1, position.y + 1, position.z - 1,
            position.x + 1, position.y + 1, position.z + 1,
            position.x + 1, position.y - 1, position.z + 1,
            // top face
            position.x + 1, position.y + 1, position.z - 1,
            position.x - 1, position.y + 1, position.z - 1,
            position.x - 1, position.y + 1, position.z + 1,
            position.x + 1, position.y + 1, position.z + 1,
            // bottom face
            position.x + 1, position.y - 1, position.z - 1,
            position.x - 1, position.y - 1, position.z - 1,
            position.x - 1, position.y - 1, position.z + 1,
            position.x + 1, position.y - 1, position.z + 1
        };
        return voxel; 
    }
    
    private float[] createVoxelVertexColor(float[] voxelColorArray) {
        float[] voxelColors = new float[voxelColorArray.length * 4 * 6];
        for (int i = 0; i < voxelColors.length; i++) {
            voxelColors[i] = voxelColorArray[i % voxelColorArray.length];
        }
        return voxelColors;
    } 
    
    private float[] getVoxelColor(Voxel voxel) {
        switch(voxel.getId()) {
            // Default
            case 1:
                return new float[] {0, 1, 0};
            // Ground
            case 2:
                return new float[] {1, 0.5f, 0};
        }
        return new float[]{1, 1, 1};
    }
    
    public void update() {
        buildMesh(startPosition);
    }
    
    public void deactivateVoxels() {
        for (int x = 0; x < VOXEL_GROUP_SIZE; x++) {
            for (int y = 0; y < VOXEL_GROUP_SIZE; y++) {
                for (int z = 0; z < VOXEL_GROUP_SIZE; z++) {
                    voxels[x][y][z].setActive(false);
                }
            }
        }
        
    }
    
    public void setupSphere() {
        for (int x = 0; x < VOXEL_GROUP_SIZE; x++) {
            for (int y = 0; y < VOXEL_GROUP_SIZE; y++) {
                for (int z = 0; z < VOXEL_GROUP_SIZE; z++) {
                    voxels[x][y][z].setActive(false);
                    double sq = Math.sqrt((double) 
                            ((x - VOXEL_GROUP_SIZE / 2) * (x - VOXEL_GROUP_SIZE / 2) +
                            (y - VOXEL_GROUP_SIZE / 2) * (y - VOXEL_GROUP_SIZE / 2) +
                            (z - VOXEL_GROUP_SIZE / 2) * (z - VOXEL_GROUP_SIZE/ 2 )) );
                    if (sq <= (VOXEL_GROUP_SIZE / 2)) {
                        voxels[x][y][z].setActive(true);
                        
                    }
                }
            }
        }
        update();
    }
    
    public void setUpLandscape() {
        deactivateVoxels();
        for (int x = 0; x < VOXEL_GROUP_SIZE; x++) {
            for (int z = 0; z < VOXEL_GROUP_SIZE; z++) {
                double noiseHeight = SimplexNoise.noise(x, z);
                int height = (int)((noiseHeight < 0 ? -noiseHeight : noiseHeight) * 20);
                //int height = 10;
                height = (height > VOXEL_GROUP_SIZE ? VOXEL_GROUP_SIZE : height);
                //LOGGER.log(Level.INFO, String.valueOf(height));
                for (int y = 0; y < height; y++) {
                    voxels[x][y][z].setActive(true);     
                }
            }
        }
        update();
        
        
    }
    
    public void cleanUp() {
        glDeleteBuffers(vboVertextHandle);
        glDeleteBuffers(vboColorHandle);  
    }
    
    public void render() {
        glPushMatrix(); 
        {
            glBindBuffer(GL_ARRAY_BUFFER, vboVertextHandle);
            glVertexPointer(3, GL_FLOAT, 0, 0L);
            glBindBuffer(GL_ARRAY_BUFFER, vboColorHandle);
            glColorPointer(3, GL_FLOAT, 0, 0L);
            glDrawArrays(GL_QUADS, 0, VOXEL_GROUP_SIZE * VOXEL_GROUP_SIZE * VOXEL_GROUP_SIZE * 24); 
        }
        glPopMatrix();
        
    }

    public Vector3f getStartPosition() {
        return startPosition;
    }
    
    
    
}
