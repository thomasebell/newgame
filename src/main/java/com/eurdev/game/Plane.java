/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eurdev.game;

import org.lwjgl.util.vector.Vector3f;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author thomas
 */
public class Plane {
    
    private Vector3f position;
    private float rotation = 0;
    
    public Plane(Vector3f position) {
        this.position = position;
    }
    
    public void draw() {
        
        
        // push matrix
        glPushMatrix(); 
        {
            // RGBA set color to blue one time
            glColor3f(1.0f, 0.0f, 0.0f);
            //glTranslatef(x, y, z);
            //glRotatef(rotation, x, y, z);
            
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//            glTranslatef(x, y, 0);
//            glRotatef(rotation, 0f, 0f, 1f);
//            glTranslatef(-x, -y, 0);

            // draw cube
            glBegin(GL_QUADS);
            {
                // front face
                glVertex3f(position.x - 2, position.x - 2, position.z - 10);
                glVertex3f(position.x - 2, position.x + 2, position.z - 10);
                glVertex3f(position.x + 2, position.x + 2, position.z - 10);
                glVertex3f(position.x + 2, position.x - 2, position.z - 10);            
                
            }
            glEnd();
        }
        glPopMatrix();
        
    }
    
}
