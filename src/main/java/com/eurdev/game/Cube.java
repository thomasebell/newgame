/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eurdev.game;
import java.util.List;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author thomas
 */
public class Cube {
    
    private Vector3f position;
    private float rotation = 0;
    private List<Float> color;

    public Cube(Vector3f position, List<Float> color) {
        this.position = position;
        this.color = color;
    }  
    
    public float getX() {
        return position.x;
    }

    public void setX(float x) {
        position.x = x;
    }

    public float getY() {
        return position.y;
    }

    public void setY(float y) {
        position.y = y;
    }

    public float getZ() {
        return position.z;
    }

    public void setZ(float z) {
        position.z = z;
    }

    public float getRotation() {
        return rotation;
    }
    
    public void draw() {
        
        
        // push matrix
        glPushMatrix(); 
        {
            glTranslatef(position.x, position.y, position.z);
            // RGBA set color
            // 0, 230, 255 light blue
            // 255, 0, 0 red
            glColor3f(color.get(0), color.get(1), color.get(2));
            
            glRotatef(rotation, position.x, position.y, position.z);
            
            //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//            glTranslatef(x, y, 0);
//            glRotatef(rotation, 0f, 0f, 1f);
//            glTranslatef(-x, -y, 0);

            // draw cube
            glBegin(GL_QUADS);
            //glBegin(GL_LINE_LOOP);
            //glBegin(GL_TRIANGLE_STRIP);
            {
                // front face
                glVertex3f(position.x - 1, position.y - 1, position.z - 1);
                glVertex3f(position.x - 1, position.y + 1, position.z - 1);
                glVertex3f(position.x + 1, position.y + 1, position.z - 1);
                glVertex3f(position.x + 1, position.y - 1, position.z - 1);
                
                // left face
                glVertex3f(position.x - 1, position.y - 1, position.z + 1);
                glVertex3f(position.x - 1, position.y + 1, position.z + 1);
                glVertex3f(position.x - 1, position.y + 1, position.z - 1);
                glVertex3f(position.x - 1, position.y - 1, position.z - 1);
                
                // back face
                glVertex3f(position.x + 1, position.y - 1, position.z + 1);
                glVertex3f(position.x + 1, position.y + 1, position.z + 1);
                glVertex3f(position.x - 1, position.y + 1, position.z + 1);
                glVertex3f(position.x - 1, position.y - 1, position.z + 1);
                
                // right face
                glVertex3f(position.x + 1, position.y - 1, position.z - 1);
                glVertex3f(position.x + 1, position.y + 1, position.z - 1);
                glVertex3f(position.x + 1, position.y + 1, position.z + 1);
                glVertex3f(position.x + 1, position.y - 1, position.z + 1);
                
                // top face
                glVertex3f(position.x + 1, position.y + 1, position.z - 1);
                glVertex3f(position.x - 1, position.y + 1, position.z - 1);
                glVertex3f(position.x - 1, position.y + 1, position.z + 1);
                glVertex3f(position.x + 1, position.y + 1, position.z + 1);
                
                // bottom face
                glVertex3f(position.x + 1, position.y - 1, position.z - 1);
                glVertex3f(position.x - 1, position.y - 1, position.z - 1);
                glVertex3f(position.x - 1, position.y - 1, position.z + 1);
                glVertex3f(position.x + 1, position.y - 1, position.z + 1);                
                
            }
            glEnd();
        }
        glPopMatrix();
        
    }
    
    public void updatePosition(float x, float y) {
        position.x = x;
        position.y = y;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }
    
    public void rotate (int delta) {
        rotation += 0.15f * delta;
    }
    
    public void moveLeft(int delta) {
        position.x += 0.015f * delta;
    }
    
    public void moveRight(int delta) {
        position.x -= 0.015f * delta;  
    }
    
    public void moveUp(int delta) {
        position.y += 0.015f * delta;  
    }
    
    public void moveDown(int delta) {
        position.y -= 0.015f * delta;  
    }
    
    public void moveForward(int delta) {
        position.z += 0.015f * delta;
    }
    
    public void moveBackward(int delta) {
        position.z -= 0.015f * delta;
    }
    
    public void turnLeft(int delta) {
        rotation += 0.1f * delta;
        
    }
    
    public void turnRight(int delta) {
        rotation -= 0.1f * delta;
    }
    
    
    
}
