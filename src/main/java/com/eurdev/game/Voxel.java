/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eurdev.game;

/**
 *
 * @author thomas
 */
public class Voxel {
    
    private boolean active;
    private VoxelType type;

    public Voxel(VoxelType type) {
        this.type = type;
    }
    
    public boolean isActive() {
        return active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }
    
    public int getId() {
        return type.getId();
    }

    public VoxelType getType() {
        return type;
    }
    
    
    
}
