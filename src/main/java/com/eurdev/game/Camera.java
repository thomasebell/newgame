/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eurdev.game;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.util.glu.GLU.*;
import org.lwjgl.util.vector.Vector3f;
/**
 *
 * @author thomas
 */
public class Camera {
    
    private Vector3f position;
    
    // rotation around X axis
    private float pitch;
    
    // rotation around Y axis
    private float yaw;
    
    // rotation around Z axis
    private float roll;
    
    private float fieldOfView;
    private float aspectRatio;
    
    private float nearClippingPlane;
    private float farClippingPlane;
    
    private float displayWidth;
    private float displayHeight;
    
    private float speed = 0.015f; 

    public Camera(Vector3f position, float yaw, float fieldOfView, float nearClippingPlane, float farClippingPlane, 
            float displayWidth, float displayHeight) {

        this.position = position;
        this.yaw = yaw;
        
        this.fieldOfView = fieldOfView;
        this.aspectRatio = displayWidth / displayHeight;
        this.nearClippingPlane = nearClippingPlane;
        this.farClippingPlane = farClippingPlane;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
        
        initProjection();
    }
    
    private void initProjection() {
        
        glScissor(0, 0, (int) displayWidth, (int) displayHeight);
        glViewport(0, 0, (int) displayWidth, (int) displayHeight);
        //glClearColor(0, 0, 0, 1);
        
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        
        gluPerspective(fieldOfView, aspectRatio, nearClippingPlane, farClippingPlane);
        
        glMatrixMode(GL_MODELVIEW);
        //glLoadIdentity();
        
        //glEnable(GL_DEPTH_TEST);   
    }
    
    public void useView() {
        glRotatef(pitch, 1, 0, 0);
        glRotatef(yaw, 0, 1, 0);
        glRotatef(roll, 0, 0, 1);
        glTranslatef(position.x, position.y, position.z);
        
    }
    
    public void moveFoward(int delta) {
        //z += 0.005f * delta;
        position.z += (speed * Math.sin(Math.toRadians(yaw + 90))) * delta;
        position.y -= (speed * Math.cos(Math.toRadians(pitch + 90))) * delta;
        position.x += (speed * Math.cos(Math.toRadians(yaw + 90))) * delta;
        
    }
    
    public void moveBackward(int delta) {
        position.z -= (speed * Math.sin(Math.toRadians(yaw + 90))) * delta;
        position.y += (speed * Math.cos(Math.toRadians(pitch + 90))) * delta;
        position.x -= (speed * Math.cos(Math.toRadians(yaw + 90))) * delta;
    }
    
    public void moveLeft(int delta) {
        position.x += (speed * Math.cos(Math.toRadians(yaw))) * delta;
        position.z += (speed * Math.sin(Math.toRadians(yaw))) * delta;
    }
    
    public void moveRight(int delta) {
        position.x -= (speed * Math.cos(Math.toRadians(yaw))) * delta;
        position.z -= (speed * Math.sin(Math.toRadians(yaw))) * delta;
    }
    
    public void moveUp(int delta) {
        position.y -= speed * delta;
    }
    
    public void moveDown(int delta) {
        position.y += speed * delta;
    }
    
    public void turnRight(int delta) {
        yaw += 0.1f * delta;
    }
    
    public void turnLeft(int delta) {
        yaw -= 0.1f * delta; 
    }
    
    public void movePitch(float amount) {
        pitch += amount;
    }
    
    public void moveYaw(float amount) {
        yaw += amount;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getRoll() {
        return roll;
    }

    public void setRoll(float roll) {
        this.roll = roll;
    }
    
    public float getFieldOfView() {
        return fieldOfView;
    }

    public void setFieldOfView(float fieldOfView) {
        this.fieldOfView = fieldOfView;
    }

    public float getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(float aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public float getNearClippingPlane() {
        return nearClippingPlane;
    }

    public void setNearClippingPlane(float nearClippingPlane) {
        this.nearClippingPlane = nearClippingPlane;
    }

    public float getFarClippingPlane() {
        return farClippingPlane;
    }

    public void setFarClippingPlane(float farClippingPlane) {
        this.farClippingPlane = farClippingPlane;
    }      
    
}
