varying vec3 normal;
varying vec3 vertexToLightVector;

void main() {
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    normal = gl_NormalMatrix * gl_Normal;

    vec4 vertexInModelViewSpace = gl_ModelViewMatrix * gl_Vertex;

    vertexToLightVector = vec3(gl_LightSource[0].position + vertexInModelViewSpace);
}